package com.llh.entity;

import java.io.Serializable;

/**
 * (Type)表实体类
 *
 * @author lilinhan
 * @since 2023-03-17 10:53:50
 */
@SuppressWarnings("serial")
public class Type {
    
    private Integer tid;
    
    private String tname;


    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    }

