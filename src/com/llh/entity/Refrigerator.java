package com.llh.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.io.Serializable;

/**
 * (Refrigerator)表实体类
 *
 * @author lilinhan
 * @since 2023-03-17 09:56:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("serial")
public class Refrigerator {
    //白酒编号
    private int rid;
    //白酒名称
    private String rname;
    //白酒香型
    private String role;
    //白酒描述
    private String detail;
    //白酒度数
    private double rdo;
    //白酒价格
    private double rpirce;
    //白酒容量
    private Integer rmall;
    //白酒生产日期
    private Date rindate;
    //白酒品牌编号
    private Integer tid;
    private String tname;


    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public String getRname() {
        return rname;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public double getRdo() {
        return rdo;
    }

    public void setRdo(double rdo) {
        this.rdo = rdo;
    }

    public double getRpirce() {
        return rpirce;
    }

    public void setRpirce(double rpirce) {
        this.rpirce = rpirce;
    }

    public Integer getRmall() {
        return rmall;
    }

    public void setRmall(Integer rmall) {
        this.rmall = rmall;
    }

    public Date getRindate() {
        return rindate;
    }

    public void setRindate(Date rindate) {
        this.rindate = rindate;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }
}

