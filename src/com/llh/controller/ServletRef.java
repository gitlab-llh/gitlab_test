package com.llh.controller;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageInfo;
import com.llh.entity.Refrigerator;
import com.llh.entity.Type;
import com.llh.service.RefrigeratorService;
import com.llh.service.impl.RefrigeratorServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet(name = "ServletRef", value = "/ref")
public class ServletRef extends HttpServlet {
    RefrigeratorService rs = new RefrigeratorServiceImpl();
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        String method = req.getParameter("m");
        if(method!=null && !"".equals(method)){
            if("getRef".equals(method)){
                getRef(req,resp);
            }
            if("toUpdate".equals(method)){
                toUpdate(req,resp);
            }
            if("getType".equals(method)){
                getType(req,resp);
            }
            if("updateRef".equals(method)){
                updateRef(req,resp);
            }
            if("deleteRef".equals(method)){
                deleteRef(req,resp);
            }
        }
    }

    private void deleteRef(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String rid = req.getParameter("rid");
        int i = rs.deleteRef(rid);
        resp.getWriter().println(JSON.toJSONString(i));
    }

    private void updateRef(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String,Object> map = new HashMap<>();
        map.put("rid",req.getParameter("rid"));
        map.put("rName",req.getParameter("rName"));
        map.put("role",req.getParameter("role"));
        map.put("detail",req.getParameter("detail"));
        map.put("rdo",req.getParameter("rdo"));
        map.put("rPirce",req.getParameter("rPirce"));
        map.put("rMall",req.getParameter("rMall"));
        map.put("rInDate",req.getParameter("rInDate"));
        map.put("tid",req.getParameter("tid"));
        int i = rs.updateRef(map);
        resp.getWriter().println(JSON.toJSONString(i));
    }

    private void getType(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<Type> type = rs.getType();
        req.setAttribute("type",type);
        resp.getWriter().println(JSON.toJSONString(type));
    }

    private void toUpdate(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String rid = req.getParameter("rid");
        Refrigerator ref = rs.toUpdate(rid);
        req.setAttribute("ref",ref);
        req.getRequestDispatcher("ref_update.jsp").forward(req,resp);
    }

    private void getRef(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pageNum = req.getParameter("pageNum");

        Map<String,Object> map = new HashMap<>();
        map.put("rName",req.getParameter("rName"));
        map.put("rPirce",req.getParameter("rPirce"));
        map.put("rInDate",req.getParameter("rInDate"));
        map.put("rPirceMax",req.getParameter("rPirceMax"));
        map.put("rInDateMax",req.getParameter("rInDateMax"));

        PageInfo<Refrigerator> pageInfo = rs.getRef(map, pageNum);
        req.setAttribute("pageInfo",pageInfo);
        req.setAttribute("map",map);
        req.getRequestDispatcher("ref_list.jsp").forward(req,resp);
    }
}
