package com.llh.service;

import com.github.pagehelper.PageInfo;
import com.llh.entity.Refrigerator;
import com.llh.entity.Type;

import java.util.List;
import java.util.Map;

/**
 * (Refrigerator)表服务接口
 *
 * @author lilinhan
 * @since 2023-03-17 09:56:31
 */
public interface RefrigeratorService {
    PageInfo<Refrigerator> getRef(Map<String,Object> map,String pageNum);
    Refrigerator toUpdate(String rid);
    List<Type> getType();
    int updateRef(Map<String,Object> map);
    int deleteRef(String rid);
}

