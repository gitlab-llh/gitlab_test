package com.llh.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.llh.dao.RefrigeratorDao;
import com.llh.entity.Refrigerator;
import com.llh.entity.Type;
import com.llh.service.RefrigeratorService;
import com.llh.utils.MyBatisUtil;

import java.util.List;
import java.util.Map;

/**
 * (Refrigerator)表服务实现类
 *
 * @author lilinhan
 * @since 2023-03-17 09:56:31
 */

public class RefrigeratorServiceImpl implements RefrigeratorService {
    @Override
    public PageInfo<Refrigerator> getRef(Map<String, Object> map, String pageNum) {
        RefrigeratorDao mapper = MyBatisUtil.getSql().getMapper(RefrigeratorDao.class);
        if(StringUtil.isEmpty(pageNum)){
            pageNum = "1";
        }
        PageHelper.startPage(Integer.parseInt(pageNum),5);
        List<Refrigerator> ref = mapper.getRef(map);
        return new PageInfo<>(ref);
    }

    @Override
    public Refrigerator toUpdate(String rid) {
        RefrigeratorDao mapper = MyBatisUtil.getSql().getMapper(RefrigeratorDao.class);
        return mapper.toUpdate(rid);
    }

    @Override
    public List<Type> getType() {
        RefrigeratorDao mapper = MyBatisUtil.getSql().getMapper(RefrigeratorDao.class);
        return mapper.getType();
    }

    @Override
    public int updateRef(Map<String, Object> map) {
        RefrigeratorDao mapper = MyBatisUtil.getSql().getMapper(RefrigeratorDao.class);
        return mapper.updateRef(map);
    }

    @Override
    public int deleteRef(String rid) {
        RefrigeratorDao mapper = MyBatisUtil.getSql().getMapper(RefrigeratorDao.class);
        return mapper.deleteRef(rid);
    }
}

