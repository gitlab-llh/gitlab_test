package com.llh.dao;

import com.llh.entity.Refrigerator;
import com.llh.entity.Type;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * (Refrigerator)表数据库访问层
 *
 * @author lilinhan
 * @since 2023-03-17 09:56:29
 */
public interface RefrigeratorDao {
    List<Refrigerator> getRef(Map<String,Object>map);
    Refrigerator toUpdate(String rid);
    List<Type> getType();
    int updateRef(Map<String,Object> map);
    int deleteRef(@Param("rid") String rid);
}

