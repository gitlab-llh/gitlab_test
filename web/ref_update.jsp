<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: lilinhan
  Date: 2023/3/17
  Time: 10:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sign-in.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript">
        $(function (){
        // （18）使用ajax实现品牌名称的下拉框追加,并回显（6分）
            $.post(
                "ref?m=getType",
                function (msg){
                    console.log(msg)
                    if(msg!=null){
                        for (let i = 0; i < msg.length; i++) {
                            $("[name=tid]").append("<option value='"+msg[i].tid+"'>"+msg[i].tname+"</option>");
                            $("[name=tid]").val(msg[i].tid);
                        }
                    }

                },
                "json"
            )
        })
    </script>
</head>
<body class="text-center">
    <main class="w-50 m-auto">
        <div class="w-75 mb-3">
            <h1 class="h3 fw-normal m-auto">修改页面</h1>
        </div>
        <form class="row g-3">
            <%--文本框 一行一框加验证--%>
            <div class="col-md-9 form-floating">
                <input type="text" value="${ref.rid}" name="rid" class="form-control" id="rid" placeholder="白酒编号" readonly>
                <label for="rid">白酒编号</label>
            </div>
            <div class="col-md-3 text-lg-start">
                <div class="badge text-wrap" id="sp1"></div>
            </div>

            <%--文本框 一行二框加验证--%>
            <div class="col-md-3 form-floating">
                <input type="text" value="${ref.rname}" name="rName" class="form-control" id="rName" placeholder="白酒名称">
                <label for="rName">白酒名称</label>
            </div>
            <div class="col-md-3 text-lg-start">
                <div class="badge text-wrap" id="sp2"></div>
            </div>
            <div class="col-md-3 form-floating">
                <input type="text" value="${ref.role}" name="role" class="form-control" id="role" placeholder="白酒香型">
                <label for="role">白酒香型</label>
            </div>
            <div class="col-md-3 text-lg-start">
                <div class="badge text-wrap" id="sp3"></div>
            </div>

            <%--文本框 一行二框加验证--%>
            <div class="col-md-3 form-floating">
                <input type="text" value="${ref.rdo}" name="rdo" class="form-control" id="rdo" placeholder="白酒度数">
                <label for="rdo">白酒度数</label>
            </div>
            <div class="col-md-3 text-lg-start">
                <div class="badge text-wrap" id="sp4"></div>
            </div>
            <div class="col-md-3 form-floating">
                <input type="text" onblur="checkPrice()" value="${ref.rpirce}" name="rPirce" class="form-control" id="rPirce" placeholder="白酒价格">
                <label for="rPirce">白酒价格</label>
            </div>
            <div class="col-md-3 text-lg-start">
                <div class="badge text-wrap" id="sp5"></div>
            </div>

            <%--文本框 一行二框加验证--%>
            <div class="col-md-3 form-floating">
<%--                （17）修改的日期使用日期插件（5分）--%>
                <input type="text" value="<fmt:formatDate value="${ref.rindate}" pattern="yyyy-MM-dd"></fmt:formatDate>" name="rInDate" class="form-control" id="rInDate" placeholder="生产日期" onclick="WdatePicker(this.value)">
                <label for="rInDate">生产日期</label>
            </div>
            <div class="col-md-3 text-lg-start">
                <div class="badge text-wrap" id="sp6"></div>
            </div>
            <div class="col-md-3 form-floating">
                <input type="text" value="${ref.rmall}" name="rMall" class="form-control" id="rMall" placeholder="白酒容量">
                <label for="rMall">白酒容量</label>
            </div>
            <div class="col-md-3 text-lg-start">
                <div class="badge text-wrap" id="sp7"></div>
            </div>

            <%--文本框 一行二框加验证--%>
            <div class="col-md-3 form-floating">
                <input type="text" value="${ref.detail}" onblur="checkDetail()" name="detail" class="form-control" id="detail" placeholder="白酒描述">
                <label for="detail">白酒描述</label>
            </div>
            <div class="col-md-3 text-lg-start">
                <div class="badge text-wrap" id="sp8"></div>
            </div>
            <div class="col-md-3 form-floating">
                <select class="form-select" id="tid" name="tid" aria-label="Floating label select example">
                    <option value="">---请选择---</option>
                </select>
                <label for="tid">请选择白酒品牌</label>
            </div>
            <div class="col-md-3 text-lg-start">
                <div class="badge text-wrap" id="sp9"></div>
            </div>

            <button class="w-75 btn btn-lg btn-primary" type="button" onclick="update()">提交</button>
        </form>
    </main>

    <script type="text/javascript">
        // （16）验证描述的文字不少于5位长度（5分）
        function checkDetail(){
            let val = $("[name=detail]").val();
            if(val.length<=5){
                $("#sp8").html("<font style='color: red'>文字不少于5</font>");
            }else{
                $("#sp8").html("");
            }
        }
        function checkPrice(){
            let val = $("[name=rPirce]").val();
            let type = val.type;
            if(type==Number){
                $("#sp5").html("<font style='color: red'>必须为数字</font>")
            }else{
                $("#sp5").html("")
            }
        }
        // （19）当符合条件后,使用ajax进行修改（5分）
        function update(){
            $.post(
                "ref?m=updateRef",
                $("form").serialize(),
                function (msg){
                    if(msg>0){
                        alert("修改成功");
                        location.href="ref?m=getRef";
                    }else{
                        alert("修改失败");
                    }
                },
                "json"
            )
        }
    </script>
</body>
</html>

