<%--
  Created by IntelliJ IDEA.
  User: lilinhan
  Date: 2023/3/17
  Time: 10:15
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>
</head>
<body>
    <center>
        <div style="width: 1300px;">
            <form action="ref?m=getRef" method="post">
                <table class="table table-bordered table-striped table-hover align-middle caption-top text-center">
                    <caption>商品列表</caption>
                    <thead class="table-light">
                        <tr>
                            <td colspan="10">
                                姓名模糊：<input type="text" name="rName" value="${map.rName}">
                                价格区间：<input type="text" name="rPirce" value="${map.rPirce}">-<input type="text" name="rPirceMax" value="${map.rPirceMax}">
                                日期区间：<input type="date" name="rInDate" value="${map.rInDate}">-<input type="date" name="rInDateMax" value="${map.rInDateMax}">

                                <button type="submit" class="btn btn-outline-primary btn-sm">查询</button>
                            </td>
                        </tr>
                        <tr>
                            <th>#</th>
                            <th>白酒名称</th>
                            <th>白酒香型</th>
                            <th>白酒描述</th>
                            <th>白酒度数</th>
                            <th>白酒价格</th>
                            <th>白酒容量</th>
                            <th>生产日期</th>
                            <th>白酒品牌</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody class="table-group-divider">
                        <c:forEach items="${pageInfo.list}" var="ref">
                            <tr class="align-bottom">
                                <th>
                                    <input type="checkbox" value="${ref.rid}" class="ck">
                                    ${ref.rid}
                                </th>
                                <td>${ref.rname}</td>
                                <td>${ref.role}</td>
                                <td>${ref.detail}</td>
                                <td>${ref.rdo}</td>
                                <td>${ref.rpirce}</td>
                                <td>${ref.rmall}</td>
                                <td><fmt:formatDate value="${ref.rindate}" pattern="yyyy-MM-dd"></fmt:formatDate></td>
                                <td>${ref.tname}</td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary btn-sm" onclick="update(${ref.rid})">修改</button>
                                </td>
                            </tr>
                        </c:forEach>
                        <tr class="table">
                            <td colspan="10">
                                <button type="button" class="btn btn-outline-primary btn-sm" onclick="qx()">全选</button>
                                <button type="button" class="btn btn-outline-primary btn-sm" onclick="qbx()">全不选</button>
                                <button type="button" class="btn btn-outline-primary btn-sm" onclick="fx()">反选</button>
                                <button type="button" class="btn btn-outline-primary btn-sm" onclick="del()">删除</button>
                                <button name="pageNum" class="btn btn-outline-primary btn-sm" value="1">首页</button>
                                <button name="pageNum" class="btn btn-outline-primary btn-sm" value="${pageInfo.prePage==1?1:pageInfo.prePage}">上一页</button>
                                <button name="pageNum" class="btn btn-outline-primary btn-sm" value="${pageInfo.nextPage==0?pageInfo.pages:pageInfo.nextPage}">下一页</button>
                                <button name="pageNum" class="btn btn-outline-primary btn-sm" value="${pageInfo.pages}">尾页</button>
                                总条数：${pageInfo.total}条&nbsp;&nbsp;当前页/总页：${pageInfo.pageNum}/${pageInfo.pages}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </center>

    <script type="text/javascript">
        function update(rid){
            location.href="ref?m=toUpdate&rid="+rid;
        }
        function qx(){
            $(".ck").each(function (){
                this.checked=true;
            })
        }
        function qbx(){
            $(".ck").each(function (){
                this.checked=false;
            })
        }
        function fx(){
            $(".ck").each(function (){
                this.checked=!this.checked;
            })
        }
        // （11）点击删除按钮使用ajax实现删除的功能（5分）
        function del(){
            var str = "";
            $(".ck:checked").each(function (){
                str += ","+$(this).val();
            })
            let s = str.substring(1);
            console.log(str)
            console.log(s)
            if(str.length>0){
                if(confirm("确定要删除吗？")){
                    $.post(
                        "ref?m=deleteRef",
                        {rid:str.substring(1)},
                        function (msg){
                            if(msg>0){
                                alert("删除成功");
                                location.href="ref?m=getRef";
                            }else{
                                alert("删除失败");
                            }
                        },
                        "json"
                    )
                }
            }else{
                alert("至少选择一个");
            }
        }
    </script>
</body>
</html>

